data "opennebula_template" "vm" {
  for_each = var.one_vms_list
  name = each.value.template_name
}

data "opennebula_virtual_network" "vm" {
  for_each = var.one_vms_list
  name = each.value.network_name
}

resource "opennebula_virtual_machine" "vm" {
  for_each = var.one_vms_list

  name        = each.value.name 
  description = each.value.description 
  cpu         = each.value.cpu
  vcpu        = each.value.vcpu
  memory      = each.value.memory
  permissions = "660"
  template_id = data.opennebula_template.vm[each.key].id

  context = {
    NETWORK      = "YES"
    HOSTNAME     = "$NAME"
  }

  graphics {
    type   = "VNC"
    listen = "0.0.0.0"
    keymap = "fr"
  }

  os {
    arch = "x86_64"
    boot = "disk0"
  }

  nic {
    model      = "virtio"
    network_id = data.opennebula_virtual_network.vm[each.key].id
  }

  tags = {
    environment = var.one_vm_environment
  }
}
