terraform {
  required_providers {
    opennebula = {
      source = "OpenNebula/opennebula"
      version = "~> 1.4"
    }
  }
}

provider "opennebula" {
  endpoint  = var.one_endpoint
  username  = var.one_username
  password  = var.one_password

  default_tags {
    tags = {
      deployment_mode = "terraform"
    }
  }
}
