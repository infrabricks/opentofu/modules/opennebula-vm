variable "one_endpoint" {
  description = "Open Nebula endpoint"
  type        = string
  sensitive   = true
}

variable "one_username" {
  description = "Open Nebula username"
  type        = string
  sensitive   = true
}

variable "one_password" {
  description = "Open Nebula password"
  type        = string
  sensitive   = true
}

variable "one_vms_list" {
  type = map(any)
  description = "List of virtual machines to be deployed"
}

variable "one_vm_environment" {
  description = "Open Nebula VM environment"
  type        = string
  default     = "dev"
}
