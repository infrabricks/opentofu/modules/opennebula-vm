# OpenNebula VM OpenTofu module

OpenTofu Module to deploy VMs in Open Nebula cluster.

## What is Open Tofu ?

Open Tofu is a fork of Terraform, an IaC tool (Infrastructure as Code).

## How to use ?

You need first add module and set variables in your main.tf project :

``` 
module "opennebula-vm" {
  source = "git::ssh://git@gitlab.mim-libre.fr:infrabricks/opentofu/modules/opennebula-vm.git"
  one_endpoint = var.one_endpoint
  one_username = var.one_username
  one_password = var.one_password
  one_vms_list = var.one_vms_list
} 
``` 

Then populate the terraform.tfvars file in your project with your credential infos and VMs infos :

``` 
one_endpoint = "https://xxxxxxxx/RPC2"
one_username = "xxxxxxxxx"
one_password = "xxxxxxxxx"

one_vm_environment = "production" # (optionnal, default: "dev")

one_vms_list = {
  vm1 = {
    name = "vmname-1"
    description = "Description"
    template_name = "template name"
    network_name = "network name"
    vcpu = 2
    cpu = 0.2
    memory = 2048
  },
  vm2 = {
    name = "vmname-2"
    description = "Description"
    template_name = "template name"
    network_name = "network name"
    vcpu = 4
    cpu = 0.4
    memory = 4096
  }
}
``` 

Init the project :

``` 
tofu init
``` 

Test your deployment steps with :

``` 
tofu plan 
``` 

When all is ready, launch your deployment with :

``` 
tofu apply 
``` 

## Links

- [OpenTofu project](https://opentofu.org/)
- [OpenNebula provider documentation](https://registry.terraform.io/providers/OpenNebula/opennebula/latest/docs)
